# Processo Seletivo Java EE #


As instruções se encontram na pasta docs.


São dois documentos ambos duplicados em formato PDF e docx:

* 01-Dev-JavaEE-InstrucoesProva

* 02-Dev-JavaEE-ConfiguracaoProjeto

O rascunho inicial da base de dados
-----------------------------------

![Base de Dados](https://bytebucket.org/erudio/lipjava-leandrocgsi/raw/cdd224bbc46d723eebe2d6dd68745411c6d78d53/docs/PlayTennisModel.jpg?token=bff4b97e914b5b6f33023322a1d9219e979c0f5b)

Acessando a documentação dos enpoints com Swagger
-------------------------------------------------

```sh
http://localhost:8080/index.html
```

![Swagger](https://bytebucket.org/erudio/lipjava-leandrocgsi/raw/cdd224bbc46d723eebe2d6dd68745411c6d78d53/docs/swagger_documentation.png?token=f568a0d1794ad846360b0f0acf39519dc00004d8)
